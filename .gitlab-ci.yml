# Add this to retrieve from Repository secret variables the SSH configuration enabling to deploy to weblab-project.org
before_script:
  # Install ssh-agent if not already installed, it is required by Docker.
  # (change apt-get to yum if you use a CentOS-based image)
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'

  # Run ssh-agent (inside the build environment)
  - eval $(ssh-agent -s)

  # Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
  - ssh-add <(echo "$SSH_PRIVATE_KEY")

  # For Docker builds disable host key checking. Be aware that by adding that you are suspectible to man-in-the-middle attacks.
  # WARNING: Use this only with the Docker executor, if you use it with shell you will overwrite your user's SSH config.
  - mkdir -p /root/.ssh
  - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > /root/.ssh/config'
  # In order to properly check the server's host key, assuming you created the SSH_SERVER_HOSTKEYS variable previously, uncomment the following two lines instead.
  - '[[ -f /.dockerenv ]] && echo "$SSH_SERVER_HOSTKEYS" > /root/.ssh/known_hosts'
  - echo "$SSH_PRIVATE_KEY" > /root/.ssh/id_rsa
  - chmod 600 /root/.ssh/id_rsa && chmod 700 /root/.ssh

  # Try to install poppler to really call tests
  - 'apt-get update -y && apt-get install poppler-utils -y'


# This file is used to configure the CI tool from GitLab (Travis CI).
# It is mainly inspired from the one provided as sample in OW2 quickstart project (https://gitlab.ow2.org/ow2-lifecycle/quick-start-template)
# Test and sonar phases have been removed since there is not a single line of in this project! 
stages:
- build
- deploy


variables:
  # This will supress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used when running from the command line.
  # `installAtEnd` and `deployAtEnd`are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true -Denv=CI --settings settings.xml"

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_REF_NAME"'
cache:
  paths:
    - .m2/repository

# Build the project using a docker image of JDK8 and Maven 3.3.9
build_job:
  stage: build
  script:
    - 'mvn $MAVEN_CLI_OPTS package'
  image: maven:3.3.9-jdk-8

# For `master` branch only run `mvn deploy`.
# Use `settings.xml` from the repository with secret variables to provide credentials to the deployment repository
deploy:
  stage: deploy
  script:
    - 'mvn $MAVEN_CLI_OPTS deploy -DperformRelease'
  only:
    - master
    - develop
  artifacts:
    paths:
    - target/
  image: maven:3.3.9-jdk-8