/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.webpage;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URI;

import javax.jws.WebService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.factory.ExceptionFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.UnexpectedException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.ow2.weblab.rdf.Value;
import org.purl.dc.elements.DublinCoreAnnotator;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.ibm.icu.text.CharsetDetector;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.document.TextDocument;
import de.l3s.boilerpipe.extractors.ArticleExtractor;
import de.l3s.boilerpipe.filters.heuristics.DocumentTitleMatchClassifier;
import de.l3s.boilerpipe.sax.BoilerpipeHTMLParser;

/**
 * This class is an implementation of a WebLab Analyser in charge of converting an HTML page into a Document.
 * It makes use of Boilerpipe, an open source library enabling to remove boilerplates from HTML files (menus, advertisements...)
 *
 * Received resources MUST be WebLab Documents annotated with a dc:format property containing 'htm' and have a wlp:hasNativeContent property pointing to an accessible file.
 *
 * @author jdoucy, ymombrun
 */
@WebService(endpointInterface = "org.ow2.weblab.core.services.Analyser")
public class WebPageExtractor implements Analyser {


	protected static final String NAME = "WebPage Extractor";


	protected static final String DEFAULT_SERVICE_URI = "weblab://web-page-extractor/";


	protected final ContentManager contentManager;


	protected final URI serviceUri;


	protected Log logger;



	/**
	 * The default constructor that calls the customised one with "weblab://web-page-extractor/" as default service URI
	 */
	public WebPageExtractor() {
		this(WebPageExtractor.DEFAULT_SERVICE_URI);
	}


	/**
	 * @param serviceUri
	 *            The uri to be used in produced by statements
	 */
	public WebPageExtractor(final String serviceUri) {
		super();
		this.logger = LogFactory.getLog(this.getClass());
		this.serviceUri = URI.create(serviceUri);
		this.contentManager = ContentManager.getInstance();
		this.logger.info("'<" + WebPageExtractor.NAME + ">' started using " + this.serviceUri.toString() + " as service URI.");
	}


	@Override
	public ProcessReturn process(final ProcessArgs args) throws UnexpectedException, InvalidParameterException, ContentNotAvailableException {
		final Document document = this.checkArgs(args);
		final String resourceUri = document.getUri();
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
		final String source = dca.readSource().firstTypedValue();
		this.logger.debug("'<" + WebPageExtractor.NAME + ">'" + " Start processing document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">'. ");
		final boolean addTitle = !(dca.readTitle().hasValue());
		try (final InputStream nativeContent = this.getContent(document)) {
			this.logger.trace("Parsing content fpr document " + document.getUri() + ".");
			final TextDocument boilerpipeParseContent = this.boilerpipeParseContent(nativeContent, document.getUri(), addTitle);
			this.logger.trace("Enriching document " + document.getUri() + " with text and title.");
			this.enrichDocument(document, boilerpipeParseContent, addTitle);
		} catch (final IOException ioe) {
			final String msg = "'<" + WebPageExtractor.NAME + ">'" + " unable to process document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">': Error parsing Content.";
			this.logger.error(msg, ioe);
			throw ExceptionFactory.createContentNotAvailableException(msg, ioe);
		}
		this.logger.debug("'<" + WebPageExtractor.NAME + ">'" + " End processing document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">'. ");

		final ProcessReturn ret = new ProcessReturn();
		ret.setResource(document);
		return ret;
	}


	/**
	 * Add a Text section and a title property (if title found in the parsed content) to the WebLab document
	 *
	 * @param document
	 *            The WebLab document into which to add a Text section and a title property
	 * @param boilerpipeParseContent
	 *            The parsed document in Boilerpipe own format
	 * @param addTitle
	 *            Whether or not to annotated extracted title
	 */
	protected void enrichDocument(final Document document, final TextDocument boilerpipeParseContent, final boolean addTitle) {
		final String boilerpipeText = boilerpipeParseContent.getText(true, false);
		final String title = boilerpipeParseContent.getTitle();
		final String resourceUri = document.getUri();
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(document);
		final String source = dca.readSource().firstTypedValue();
		if ((boilerpipeText != null) && !boilerpipeText.trim().isEmpty()) {
			final Text text = WebLabResourceFactory.createAndLinkMediaUnit(document, Text.class);
			text.setContent(boilerpipeText);
			new WProcessingAnnotator(text).writeProducedBy(this.serviceUri);
		} else {
			this.logger.warn("'<" + WebPageExtractor.NAME + ">'" + " Unable to properly process document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">'. Unable to extract text content.");
		}
		if ((title != null) && !title.trim().isEmpty() && addTitle) {
			dca.writeTitle(title);
		}
	}


	/**
	 * Parses the file using BoilerpipeHTMLParser and removes boilerplate using the article instance.
	 *
	 * @param stream
	 *            The file to be parsed
	 * @param docUri
	 *            The URI of the document to be enriched
	 * @param addTitle
	 *            Whether or not to annotated extracted title
	 * @return The normalised text parsed from the file
	 * @throws ContentNotAvailableException
	 *             If an {@link IOException} occurs when accessing and parsing file
	 * @throws UnexpectedException
	 *             If a {@link SAXException} occurs when parsing file
	 */
	protected TextDocument boilerpipeParseContent(final InputStream stream, final String docUri, final boolean addTitle) throws ContentNotAvailableException, UnexpectedException {
		final BoilerpipeHTMLParser parser = new BoilerpipeHTMLParser();
		try (final BufferedInputStream bufferedInputStream = new BufferedInputStream(stream); final Reader fd = new CharsetDetector().getReader(bufferedInputStream, null)) {
			parser.parse(new InputSource(fd));
		} catch (final FileNotFoundException fnfe) {
			final String msg = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document " + "'<" + docUri + ">'. " + "Unable to open a FileReader on Content of Document " + docUri + ".";
			this.logger.error(msg, fnfe);
			throw ExceptionFactory.createContentNotAvailableException(msg, fnfe);
		} catch (final SAXException saxe) {
			final String msg = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document " + "'<" + docUri + ">'. " + "Unable to parse Content of Document " + docUri + ".";
			this.logger.error(msg, saxe);
			throw ExceptionFactory.createUnexpectedException(msg, saxe);
		} catch (final IOException ioe) {
			final String msg = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document " + "'<" + docUri + ">'. " + "Unable to parse Content of Document " + docUri + ".";
			this.logger.error(msg, ioe);
			throw ExceptionFactory.createContentNotAvailableException(msg, ioe);
		}

		final TextDocument textDocument = parser.toTextDocument();

		if (addTitle) {
			final DocumentTitleMatchClassifier documentTitleMatchClassifier = new DocumentTitleMatchClassifier(textDocument.getTitle());
			try {
				documentTitleMatchClassifier.process(textDocument);
			} catch (final BoilerpipeProcessingException bpe) {
				this.logger.warn("'<" + WebPageExtractor.NAME + ">'" + " unable to properly process document " + "'<" + docUri + ">'. "
						+ "Could not transform blocks containing parts of the HTML <TITLE> tag as content blocks for Content of Document " + docUri + ".", bpe);
			}
		}

		try {
			ArticleExtractor.INSTANCE.process(textDocument);
		} catch (final BoilerpipeProcessingException bpe) {
			final String msg = "'<" + WebPageExtractor.NAME + ">'" + " error processing document " + "'<" + docUri
					+ ">'. Problem trying to mark TextDocument using ArticleExtractor. Stopping the process...";
			this.logger.error(msg, bpe);
			throw ExceptionFactory.createUnexpectedException(msg, bpe);
		}

		return textDocument;
	}


	/**
	 * Get the document inside the process args or throw an <code>InvalidParameterException</code> if not valid (it must be a Document annotated with a dc:format containing 'htm'.
	 *
	 * @param args
	 *            The <code>ProcessArgs</code> of the process method.
	 * @return The <code>Document</code> that must be contained by <code>args</code>.
	 * @throws InvalidParameterException
	 *             If <code>args</code> is <code>null</code>, if <code>resource</code> in <code>args</code> is <code>null</code> or not a <code>Document</code> and if no htm based dc:format property
	 *             is found.
	 */
	protected Document checkArgs(final ProcessArgs args) throws InvalidParameterException {
		if (args == null) {
			final String err = "'<" + WebPageExtractor.NAME + ">' error processing document: ProcessArgs is null.";
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException(err);
		}
		final Resource res = args.getResource();
		if (res == null) {
			final String err = "'<" + WebPageExtractor.NAME + ">' error processing document: Resource is null.";
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException(err);
		}
		if (!(res instanceof Document)) {
			final String err = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document. Resource must be a document, detected type: " + res.getClass() + ".";
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException(err);
		}
		final Value<String> format = new DublinCoreAnnotator(res).readFormat();
		if (!format.hasValue() || !format.firstTypedValue().toLowerCase().contains("htm")) {
			final String err = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document. Resource must be have a dc:format annotation which contains 'htm'.";
			this.logger.error(err);
			throw ExceptionFactory.createInvalidParameterException(err);
		}
		return (Document) res;
	}


	/**
	 * Uses the content manager to retrieve the native content of the document in input.
	 *
	 * @param document
	 *            The document that must contains an hasNativeContentProperty
	 * @return The input stream opened on the content
	 * @throws ContentNotAvailableException
	 *             If the ContentManager fails or if the file does not exist or is not accessible.
	 */
	protected InputStream getContent(final Document document) throws ContentNotAvailableException {
		final Value<URI> nativeContents = new WProcessingAnnotator(document).readNativeContent();
		final String resourceUri = document.getUri();
		final String source = new DublinCoreAnnotator(document).readSource().firstTypedValue();

		if (!nativeContents.hasValue()) {
			final String err = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document. No native content annotation on document: " + document.getUri();
			this.logger.error(err);
			throw ExceptionFactory.createContentNotAvailableException(err);
		}

		InputStream inputStream = null;
		for (final URI nativeContent : nativeContents.getValues()) {
			final File localFile = this.contentManager.readLocalExistingFile(nativeContent, document, null);
			if (localFile == null) {
				try {
					inputStream = this.contentManager.read(nativeContent);
				} catch (final IOException ioe) {
					this.logger.warn(
							"'<" + WebPageExtractor.NAME + ">' error reading content " + nativeContent.toASCIIString() + ": Document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">'.",
							ioe);
					continue;
				}
			} else {
				try {
					inputStream = new FileInputStream(localFile);
				} catch (final FileNotFoundException fnfe) {
					this.logger.warn("'<" + WebPageExtractor.NAME + ">' error opening " + localFile.getAbsolutePath() + ": Document " + "'<" + resourceUri + ">'" + " - " + "'<" + source + ">'.",
							fnfe);
					continue;
				}
			}
			break;
		}

		if (inputStream == null) {
			final String err = "'<" + WebPageExtractor.NAME + ">'" + " Error processing document. No valid native content on document: " + resourceUri;
			this.logger.error(err);
			throw ExceptionFactory.createContentNotAvailableException(err);
		}

		return inputStream;
	}

}
