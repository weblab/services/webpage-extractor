/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2018 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.service.webpage;

import java.io.File;
import java.io.FileInputStream;
import java.net.URI;

import org.junit.Assert;
import org.junit.Assume;
import org.junit.Test;
import org.ow2.weblab.content.api.ContentManager;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.Document;
import org.ow2.weblab.core.model.MediaUnit;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.core.model.processing.WProcessingAnnotator;
import org.ow2.weblab.core.services.ContentNotAvailableException;
import org.ow2.weblab.core.services.InvalidParameterException;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.analyser.ProcessReturn;
import org.purl.dc.elements.DublinCoreAnnotator;



public class WebPageExtractorTest {


	/**
	 *
	 */
	private static final String TEXT_HTML = "text/html";


	@Test(expected = InvalidParameterException.class)
	public void testNullProcessArgs() throws Exception {
		new WebPageExtractor().process(null);
	}


	@Test(expected = InvalidParameterException.class)
	public void testNullResource() throws Exception {
		new WebPageExtractor().process(new ProcessArgs());
	}


	@Test(expected = InvalidParameterException.class)
	public void testNotADocument() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(WebLabResourceFactory.createResource("test", "NotADocument", Resource.class));
		new WebPageExtractor().process(pa);
	}


	@Test(expected = InvalidParameterException.class)
	public void testNoFormat() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		pa.setResource(WebLabResourceFactory.createResource("test", "NoFormat", Document.class));
		new WebPageExtractor().process(pa);
	}


	@Test(expected = InvalidParameterException.class)
	public void testBadFormat() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "BadFormat", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat("text/plain");
		new WebPageExtractor().process(pa);
	}


	@Test(expected = ContentNotAvailableException.class)
	public void testNoContent() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "NoContent", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat(WebPageExtractorTest.TEXT_HTML);
		new WebPageExtractor().process(pa);
	}


	@Test(expected = ContentNotAvailableException.class)
	public void testRemovedHTMLFile() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "RemovedHTMLFile", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat(WebPageExtractorTest.TEXT_HTML);
		final URI uri;
		try (final FileInputStream fis = new FileInputStream("src/test/resources/test.html")) {
			uri = ContentManager.getInstance().create(fis);
		}
		new WProcessingAnnotator(doc).writeNativeContent(uri);
		final File file = new File(uri);
		Assume.assumeTrue(file.delete());
		new WebPageExtractor().process(pa);
	}


	@Test(expected = ContentNotAvailableException.class)
	public void testNotAFile() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "NotAFile", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat(WebPageExtractorTest.TEXT_HTML);
		final URI uri;
		try (final FileInputStream fis = new FileInputStream("src/test/resources/test.html")) {
			uri = ContentManager.getInstance().create(fis);
		}
		new WProcessingAnnotator(doc).writeNativeContent(uri);
		final File file = new File(uri);
		Assume.assumeTrue(file.delete());
		Assume.assumeTrue(file.mkdir());
		new WebPageExtractor().process(pa);
	}


	@Test
	public void testNormalFile() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "NormalFile", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat(WebPageExtractorTest.TEXT_HTML);
		final URI uri;
		try (final FileInputStream fis = new FileInputStream("src/test/resources/test.html")) {
			uri = ContentManager.getInstance().create(fis);
		}
		new WProcessingAnnotator(doc).writeNativeContent(uri);
		final ProcessReturn pr = new WebPageExtractor().process(pa);
		Assert.assertEquals(doc, pr.getResource());
		Assert.assertEquals(1, doc.getMediaUnit().size());
		final MediaUnit mu = doc.getMediaUnit().get(0);
		Assert.assertEquals(Text.class, mu.getClass());
		Assert.assertTrue(((Text) mu).getContent().trim().endsWith("The text"));
		Assert.assertTrue(new DublinCoreAnnotator(doc).readTitle().hasValue());
		Assert.assertEquals(1, new DublinCoreAnnotator(doc).readTitle().size());
		Assert.assertEquals("The title", new DublinCoreAnnotator(doc).readTitle().firstTypedValue());
		new WebLabMarshaller(true).marshalResource(doc, new File("target/NormalFile.xml"));
	}


	@Test
	public void testNormalFileWithoutHandlingTitle() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "NormalFile2", Document.class);
		pa.setResource(doc);
		final DublinCoreAnnotator dca = new DublinCoreAnnotator(doc);
		dca.writeFormat(WebPageExtractorTest.TEXT_HTML);
		final String inputTitle = "Do not extract title!";
		dca.writeTitle(inputTitle);
		final URI uri;
		try (final FileInputStream fis = new FileInputStream("src/test/resources/test.html")) {
			uri = ContentManager.getInstance().create(fis);
		}
		new WProcessingAnnotator(doc).writeNativeContent(uri);
		final ProcessReturn pr = new WebPageExtractor().process(pa);
		Assert.assertEquals(doc, pr.getResource());
		Assert.assertEquals(1, doc.getMediaUnit().size());
		final MediaUnit mu = doc.getMediaUnit().get(0);
		Assert.assertEquals(Text.class, mu.getClass());
		Assert.assertTrue(((Text) mu).getContent().trim().endsWith("The text"));
		Assert.assertTrue(new DublinCoreAnnotator(doc).readTitle().hasValue());
		Assert.assertEquals(1, new DublinCoreAnnotator(doc).readTitle().size());
		Assert.assertEquals(inputTitle, dca.readTitle().firstTypedValue());
		new WebLabMarshaller(true).marshalResource(doc, new File("target/NormalFile.xml"));
	}


	@Test
	public void testFrameFile() throws Exception {
		final ProcessArgs pa = new ProcessArgs();
		final Document doc = WebLabResourceFactory.createResource("test", "FrameFile", Document.class);
		pa.setResource(doc);
		new DublinCoreAnnotator(doc).writeFormat(WebPageExtractorTest.TEXT_HTML);
		final URI uri;
		try (final FileInputStream fis = new FileInputStream("src/test/resources/testFrames.htm")) {
			uri = ContentManager.getInstance().create(fis);
		}
		new WProcessingAnnotator(doc).writeNativeContent(uri);
		final ProcessReturn pr = new WebPageExtractor().process(pa);
		Assert.assertEquals(doc, pr.getResource());
		Assert.assertFalse(doc.isSetMediaUnit());
		Assert.assertEquals("The frame title", new DublinCoreAnnotator(doc).readTitle().firstTypedValue());
		new WebLabMarshaller(true).marshalResource(doc, new File("target/FrameFile.xml"));
	}

}
